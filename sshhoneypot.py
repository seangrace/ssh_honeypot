#!/usr/bin/python
import socket,thread,threading,paramiko,pygeoip,time


def main():
	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	try:
		s.bind(('',22))
	except socket.error:
		print "Error creando el socket, comprueba que el puerto no este en uso!!"
		exit(0)	
	s.listen(100)
	paramiko.util.log_to_file("logs/filename.log")
	while True:
		c,addr=s.accept() # Recibimos el socket para enviar y revibir
						  # y el addr que es la ip
		thread.start_new_thread(conexion,(c,addr)) # Iniciamos un hilo

def conexion(c,addr):
	try:
		rsakey=paramiko.RSAKey(filename='ssh.key')
	except IOError:
		print "Genera la clave RSA, Ej: ssh-keygen -t rsa -f ssh.key"
		exit(0)	
	t = paramiko.Transport(c)
	t.add_server_key(rsakey)
	sshserver = SSHServer(addr)
	try:
		t.start_server(server=sshserver)
	except :
		print "Error"	
	channel = t.accept(1)
	if not channel is None:
		channel.close()

class SSHServer(paramiko.ServerInterface):
	def __init__(self,addr):
		self.event = threading.Event()
		self.addr = addr
	def check_auth_password(self, username, password):
		log = username + ":" + password + ' ==> ip:' + self.addr[0] + '\n'
		print log
		f = open('logs/sshlog.txt','a')
		f.write(log)
		f.close()
		thread.start_new_thread(checkCredentials,(self.addr,username,password))
		#checkCredentials(self.addr,username,password)
		time.sleep(1)
		return paramiko.AUTH_FAILED

def checkCredentials(addr,user,password):
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	try:
		client.connect(addr[0],username=user,password=password)
	except paramiko.ssh_exception.AuthenticationException:
		print "Credenciales incorrectas"
		return
	except socket.error:
		return	
	print "Credenciales Correctas"
	logindata = user+':'+password+' ==> ip:'+addr[0]+'\n'
	f = open('logs/validcredentials.txt','a')
	f.write(logindata)
	f.close()

def generateStats():
	print "\nGenerando estadisticas en stats.txt ..."
	try:
		f = open('logs/sshlog.txt','r')
	except IOError:
		print "No se existen logs para generas los stats"
		os.kill	
	linea = f.readline()
	listaip = []
	contador = []
	numeropet = 0
	x = 0
	while not linea == "":
		linea = linea.split("ip:")[1]
		for ip in listaip:
			if ip == linea:
				contador[x]+=1
				break
			x += 1	
		if x >= len(listaip):
			listaip.append(linea)
			contador.append(1)
		x = 0
		linea = f.readline()	 	 
	f.close()
	f = open('logs/stats.txt','w')
	x = 0
	listaip = sorted(listaip)
	for ip in listaip:
		log = 'IP: ==> '+ ip.split('\n')[0] + ' [' + locateIp(ip) + ']\n Numero de peticiones: ' + str(contador[x]) + '\n'
		f.write(log)
		numeropet += contador[x]
		x += 1
	f.write('\nNumero total de peticiones: '+ str(numeropet) + ' desde '+ str(len(listaip)) + ' direcciones IP ' +'\n')
	f.close()

def generateDic():
	print "\nGenerando diccionario en diccionario.txt ...\n"
	try:
		f = open('logs/sshlog.txt','r')
	except IOError:
		print "No se existen logs para generar el diccionario"
		exit(0)	
	linea = f.readline()
	logins = []
	x = 0
	while not linea == "":
		linea = linea.split("==>")[0] + '\n'
		for log in logins:
			if log == linea:
				break
			x += 1	
		if x >= len(logins):
			logins.append(linea)
		x = 0
		linea = f.readline()	 	 
	f.close()
	f = open('logs/diccionario.txt','w')
	x = 0
	logins = sorted(logins)
	for log in logins:
		f.write(log)
	f.close()	

def locateIp(ip):
	geoip = pygeoip.GeoIP('geoip/GeoIP.dat')
	country = geoip.country_name_by_addr(ip)
	return country
try:
	main()
except KeyboardInterrupt:
	generateStats()
	generateDic()
	
