# SSH-HoneyPot #



### What is this repository for? ###

You create a SSH Honey Pot where you store all the tries from an attacker that's trying to get access to your network.
Some of the features:
 -Make a dictionary with all the tries they have done
 -Try back against the attacker the same user:pass combination and store them if they work
 -Make a stats file with the number of requests from each IP 


### How do I get set up? ###

You need to generate a key pair for the ssh protocol, so you can use this:

```
#!bash

ssh-keygen -t rsa -f ssh.key
```

Install python libraries paramiko,pygeoip

```
#!bash

sudo apt-get install build-essential libssl-dev libffi-dev python-dev

pip install paramiko

pip install pygeoip
```


Then you must stop the ssh service if running in your machine 
And finally execute the script:


   
```
#!bash

 python sshhoneypot.py
```